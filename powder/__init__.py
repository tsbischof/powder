from xml.etree import ElementTree

import spectrum_analysis

def spectrum_from_pdf_xml(filename):
    """Given an xml-format pdf file, return the peaks as pairs of
angle and intensity."""
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    sticks = root.findall("graphs")[0].findall("stick_series")[0]

    angles = list()
    counts = list()

    for stick in sticks:
        angles.append(float(stick.find("theta").text))
        counts.append(float(stick.find("intensity").text))
    
    return(spectrum_analysis.AngleSpectrum(angles, counts))
