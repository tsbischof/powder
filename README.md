# powder
Python interface to ICDD PDF format files. 

Currently, this package is limited to parsing the xml-format PDF file to return the stick spectrum (reference peaks)

## Usage
To obtain the stick spectrum from an XML PDF file:

	import powder

	spectrum = powder.spectrum_from_xml_pdf(filename)
	print(spectrum.angles, spectrum.counts)

## Issues
There is a wiki and issue tracker for this project.
